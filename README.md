# 人脸情绪分析程序

> vue-electron client.
> 技术栈：Face++ API Node.js Vue.js Electron-vue.


#### 克隆项目

``` bash
git clone https://gitee.com/jiasichen/vue-electron-faceRecognition-client.git
```

#### 安装

``` bash
# 国内安装
cnpm install 或 yarn install

# 启动项目 serve with hot reload at localhost:9080
npm run dev 或 yarn run dev

# 打包项目 build electron application for production
npm run build 或 yarn run build

# lint all JS/Vue component files in `src/`
npm run lint
```

#### 修改请求参数

``` bash
# 替换Face++ 请求参数
/src/renderer/components/LandingPage.vue 中 'api_key'和'api_secret' 这两个参数替换为你自己的key,获取方法在Face++官网中有介绍。
```

> 开始使用吧

---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).
